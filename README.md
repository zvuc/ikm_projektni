# IKM Projektni zadatak 

**Izvještaj za projektni zadatak iz predmeta Industrijske komunikacione mreže**

**Profesor:**  

- doc. dr Mladen Knežić

**Studenti:**  

- Minja Žerić, br. indeksa 1202/17  
- Žarko Vučenović, br. indeksa 1230/18

**Zadatak**: U okviru SocketCAN interfejsa upoznati se i realizovati CAN_BCM (Broadcast Manager) tip socket-a.

## Uvod 

U okviru SocketCAN interfejsa za pristup CAN interfejsu u sklopu Linux operativnog sistema, postoji nekoliko tipova socket-a koje je moguće koristiti. CAN (Controller Area Network) je mrežna tehnologija koja se široko koristi u automatizaciji, automobilskim sistemima i ugrađenim (embedded) sistemima.  
Postoje i druge CAN implementacije, ali ovdje će se koristiti SocketCAN koji koristi Berklijev socket API. API CAN socket je dizajniran da bude što sličniji TCP/IP protokolu, tako da programeri koji su obučeni za mrežno programiranje mogu lako da nauče i savladaju CAN socket.  
 Dva bitnija tipa socket-a koja možemo izdvojiti su CAN_RAW i CAN_BCM (Broadcast Manager). U okviru ovog teksta, analiziraćemo CAN_BCM tipa socket-a, koji, između ostalog, podržava efikasnije slanje cikličnih okvira.

## Prvi korak - preuzimanje i konfigurisanje paketa

Prvo je potrebno klonirati odgovarajući repozitorijum projektnog zadatka:

>git clone https://bitbucket.org/zvuc/ikm_projektni.git

Naredni korak obuhvata kloniranje potrebnih alata u radni direktorijum:

>git clone --depth=1 https://github.com/linux-can/can-utils.git

Nakon preuzimanja odgovarajućeg repozitorijuma potrebno je izvršiti kroskompajliranje biblioteke. U ovom projektu koriste se alati za automatizovano kompajliranje projekata. Prvo je potrebno napraviti dodatni folder (npr. usr) u radnom direktorijumu gdje će se kasnije nalaziti prekompajlirana verzija biblioteke sa kojom će se kasnije dinamički linkovati izvršni fajl aplikacije.

>mkdir usr

Sljedeći korak podrazumijeva da pređemo u folder u kojem se nalazi repozitorijum *can-utilis* projekta i dalje je potrebno pokrenuti određeni set komandi za konfiguraciju *build* sistema i kompajliranje projekta.

>cd can-utils

Prije nego što pređemo u naredni korak, potrebno je provjeriti da li imamo odgovarajuće alate za dalje automatizovano kompajliranje projekata (tzv. automake *build* system). U konkretnom slučaju potrebni su nam alati automake, autoconf i libtool i u terminalu možemo provjeriti da li imamo navedene alate unosom sljedeće komande:
 
>dpkg -l automake autoconf libtool 

Ukoliko neki od alata nije instaliran, potrebno ih je instalirati unosom komande:

>  sudo apt-get install -y automake autoconf libtool

Nakon toga, možemo izvršiti dalje komande potrebne za konfiguraciju projekta.

>./autogen.sh

U narednoj komandi sa *prefix* definišemo putanju gdje želimo instalirati pakete (navodimo putanju do usr foldera) i host platformu koju koristimo, odnosno želimo kroskompajlirati.

>./configure --prefix=/path/to/usr --host=arm-linux-gnueabihf

Nakon ovoga pokrećemo komande *make* i *make install*

>make  

>make install

Nakon ovih komandi, kao rezultat, u okviru direktorijuma usr dobijamo binarne fajlove koji su sastavni dio *can-utilis* projekta.

Potrebno je dodatno kopirati candump iz foldera usr na ciljnu platformu.


## Konfigurisanje CAN drajvera na ciljnoj platformi

Ukoliko želimo da koristimo CAN interfejs na *Raspberry Pi* platformi, neophodno je da obezbjedimo odgovarajući hardverski modul koji je povezan na jedan od interfejsa koje ova platforma nudi. Potrebno je da podesimo *Raspberry Pi* platformu da koristi CAN infrastrukuru, odnosno da se poveže odgovarajući drajver sa odgovarajućim CAN kontrolerom i da se omogući komunikacija između dva čvora po CAN protokolu. U ovom slučaju koristimo modul *CAN SPI* proizvođača Mikroelektronika i koji sadrži CAN MCP2515 kontroler i MCP2515 transiver, a koji se povezuju na *Raspberry Pi* platformu preko *SPI* interfejsa. 

Za MCP2515 CAN kontroler već postoji podrška u okviru Linux operativnog sistema u vidu drajverskog modula. Da bismo učitali odgovarajući drajver koji komunicira sa MCP2515 kontrolerom, potrebno je da to omogućimo na nižem nivou *Raspberry Pi* platforme tako da se prilikom podizanja sistema povuče odgovarajući drajver. Unosom komande

>ls /boot/overlays

Možemo vidjeti koje sve overlay-eve imamo, a nama je potreban specifičan mcp2515-can2.dtbo overlay za komunikaciju preko *SPI* interfejsa, stoga ga je potrebno prebaciti na ciljnu platformu, jer se tu ne nalazi podrazumijevano. Da bi se ovaj modul učitao prilikom podizanja sistema na *Raspberry Pi* platformi, potrebno je promijeniti strukturu hardvera sistema definisanjem odgovarajućih parametara u okviru /boot/config.txt fajla. Potrebno je unijeti sljedeću komandu (potrebno je raditi sa administratorskim privilegijama zato koristimo naredbu *sudo*):

>sudo nano /boot/config.txt

Na kraju fajla potrebno je unijeti sljedeće komande:

>dtparam=spi=on
>dtoverlay=spi1-3cs
>dtoverlay=mcp2515-can2,oscillator=10000000,spimaxfrequency=1000000,interrupt=25

Nakon podešavanja ovih parametara potrebno je uraditi *reboot* samog *Raspberry Pi*, a potom je potrebno da verifikujemo da li je drajver ispravno učitan, a to radimo sa narednom komandom:

>dmesg | grep can
 
Kao rezultat pokretanja ove narebe, ukoliko je sve ispravno, trebalo bi da dobijemo sljedeći ispis na konzoli:

>[    4.112571] mcp251x spi0.0 can0: MCP2515 successfully initialized.

Ako se prijavi neka greška, potrebno je provjeriti da li je CAN SPI click modul, odnosno namjenska dodatna pločica, ispravno povezan sa SPI interfejsom Raspberry Pi platforme.

Sljedeći korak u omogućenju samog mrežnog interfejsa CAN kontrolera jeste da se podigne mrežni interfejs. Ukoliko naredbom *ifconfig* izlistamo sve mrežne interfejse vidimo da nema CAN mrežnog interfejsa. Da bismo aktivirali taj mrežni interfejs potrebno je da unesemo sljedeću komandu:

>sudo ip link set can0 up type can bitrate 125000

Ukoliko nakon ove komande ponovo unesemo komandu *ifconfig* vidimo da nam se pojavio i can0 interfejs. Sa komandom *ip link show* prikazujemo sve interfejse koji su trenutno dostupni, bilo da su aktivirani ili ne. Komanda *ifconfig* prikazuje trenutno aktivne interfejse. Deaktiviranje interfejsa se obavlja sljedećom komandom (nju naravno sada ne izvršavamo):

>sudo ip link set can0 down

Ukoliko ponovo pokrenemo komandu

>dmesg | grep can

vidjećemo da nam je aktivan can0 mrežni interfejs i komunikacija u sistemu može da počne.

Kako su nam za komunikaciju potrebna dva čvora, ova sva podešavanja moramo ponoviti i na drugom *Raspberry Pi* uređaju. Nakon toga potrebno je da se CAN_HI i CAN_LOW signalne linije CAN magistrale povežu između ova dva čvora i da se na određeni način terminišu sa otpornicima da bi se izbjegla refleksija i komunikacija može da počne. Komunikacija između čvorova se obavlja tako što se u terminalu na prijemnoj i predajnoj strani pokrenu sljedeće komande:

>bcm_send can0 

>candump can0 //pokreće se u drugom terminalu


## Opcode i flag opcije prilikom korištenja CAN_BCM (*Ograničeno na one korištene u demonstraciji*)

**Opcode:**  

1. **TX_SETUP** : Kreiranje (cikličnog) slanja  
2. **RX_SETUP** : Kreiranje filtera sadržaja   
3. **RX_CHANGED** : BCM poruka sa update-ovanim CAN frejmom  

**Flegovi:**

1. **SETTIMER** : Podešavanje vrijednosti ival1, ival2 i count (elementi strukture bcm_msg_head)  
2. **STARTTIMER** : Postavljanje tajmera na vrijednosti ival1, ival2 i count  
3. **RX_FILTER_ID** : Filtriranje samo na osnovu can_id (ne gledaju se podaci)  



## Konfigurisanje CAN_BCM (Broadcast Manager) socket-a

Za potrebe CAN infrastrukture, realizovan je poseban *SocketCAN* aplikacioni interfejs, koji je kompatibilan sa *BSD Sockets* interfejsom. Aplikacioni interfejs [*BSD Sockets*]( https://en.wikipedia.org/wiki/Berkeley_sockets ) je standardizovan interfejs za pristup mrežnim uređajima s ciljem prenosa podataka. Konceptualno je zamišljen kao apstrakcija mrežnog interfejsa, odnosno skupa protokola koji se na tom interfejsu koriste, a reprezentuje se tzv. socket objektom na dva krajnja čvora koji treba da komuniciraju. Da bi se omogućila komunikacija čvorova, njihove aplikacije treba da se "priključe" na *socket*, nakon čega se podaci mogu razmjenjivati standardnim sistemskim pozivima. Osim komunikacije između umreženih uređaja, *BSD Sockets* interfejs se koristi i kao efikasan mehanizam za komunikaciju između različitih procesa unutar istog računara.
Prvi korak u radu jeste kreiranje i otvranje odgovarajućeg socket-a

	s = socket(PF_CAN, SOCK_DGRAM, CAN_BCM);

Nakon toga, potrebno je povezati *socket* sa mrežnim interfejsom (u našem slučaju to je CAN mrežni interfejs), kako bi znao kome se podaci šalju i to omogućavamo narednom funkcijom:

	connect(s, (struct sockaddr *)&addr, sizeof(addr))

Dalje je neophodno da se na odgovarajući način popune polja `sturct sockadr` strukture. Inače, ovu strukturu možemo uključiti u zaglavlju, jer se nalazi u okviru fajla <linux/can.h>.

	struct sockaddr_can {
            sa_family_t can_family;
            int         can_ifindex;
            union {
                    /* transport protocol class address info (e.g. ISOTP) */
                    struct { canid_t rx_id, tx_id; } tp;
                    /* reserved for future CAN protocols address information */
            } can_addr;
    };

U suštini, kao i kod podešavanja CAN_RAW okvira,  dovoljno je podesiti polja can_family i can_ifindex. I ovdje se tipično prvo polje podešava da bude AF_CAN, dok je za drugo polje neophodno pribaviti indeks na osnovu naziva mrežnog interfejsa. Ovo se može obaviti ioctl() sistemskim pozivom na sljedeći način (struktura ifreq je uključena u zaglavlju a nalazi se u okviru fajla <net/if.h>):

	int s,i;
	stuct sockaddr_can addr;
	struct ifreq ifr;
	(--)
	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);
	(..)
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

Prije slanja poruka, neophodno je da se popune odgovarajuća polja strukture can_msg gdje su sadržane informacije o porukama koje šaljemo, a to se sve nalazi u strukturama bcm_msg_head i can_frame i koje su uključene u zaglavlja, a nalaze se unutar fajlova <linux/can/bcm.h> i <linux/can.h>, respektivno. Napomena, vrijednost NFRAMES predstavlja broj frejmova koje šaljemo i definisana je kao makro na početku samog fajla. 

	struct can_msg
	 {
		struct bcm_msg_head msg_head;
		struct can_frame frame[NFRAMES];
	 } msg;

Sa TX_SETUP kreiramo ciklične taskove i dalje omogućavamo slanje poruka. Od interesa su nam polja can_id, koje određuje identifikator CAN okvira, polje nframes koje određuje broj frejmova, zatim polja ival2.tv_sec i ival2.tv_usec koja definišu tačno vrijeme ponavljanja slanja naredne poruke, odnosno definišu vrijeme cikličnog salnja okvira.

	msg.msg_head.opcode  = TX_SETUP;
	msg.msg_head.can_id  = 0;
	msg.msg_head.flags   = SETTIMER | STARTTIMER;
	msg.msg_head.nframes = NFRAMES;
	msg.msg_head.count   = 0;
	msg.msg_head.ival2.tv_sec = 1;
	msg.msg_head.ival2.tv_usec = 200000;

U nastavku je prikazan način slanja poruka gdje ih na prijemnoj strani uz pomoć candump možemo očitati. Od interesa nam je i polje can_dlc, koje definiše dužinu poruke izraženo brojem bajtova, i niz bajtova data u kojem se nalaze sami podaci.
    	
	for (i = 0; i < NFRAMES; ++i)
	 {
	 	struct can_frame * const frame = msg.frame + i;
		frame->can_id = MSGID + i;
		frame->can_dlc = MSGLEN;
		memset(frame->data, i, MSGLEN);
	 }

Korišćenjem standardnih sistemskih poziva write() i read(), možemo da šaljemo i primamo podatke preko ovog mrežnog interfejsa.
   	
	if (write(s, &msg, sizeof(msg)) < 0)
	 {
		printf("Neuspjesno pisanje\n");
	 }

Dalje možemo omogučiti beskonačno slanje poruka, narednim dijelom koda.

	int k=0;
	while (0 == k) usleep(DELAY);

Po završetku rada sa *SocketCAN* interfejsom, potrebno je pozivom funkcije close() osloboditi resurse i zatvoriti prethodno kreirani socket.

	close(s);


### Konfigurisanje prijema 

Princip konfigurisanja sa prijemne strane isti je kao i kod predajne samo što se koriste **RX** operacije. 
Strukture koje su protrebne za realizaciju i način povezivanja socket-a su identične te neće biti navedene ovdje. 

## Čitanje i ispis primljenog frejma

Za ispis je kreirana funkcija *print_can_frame()* definisana sa: 

	void print_can_frame(const struct can_frame * const frame)
	{
	    const unsigned char *data = frame->data;
	    const unsigned int dlc = frame->can_dlc;
	    unsigned int i;
	    (..)
	    printf("%03X  [%u] ", frame->can_id, dlc);
	    for (i = 0; i < dlc; ++i)
	    {
	        printf(" %02X", data[i]);
	    }
	}


Čitanje primljenog frejma vrši se pomoću funkcije *read()*  

	read(s, &msg, sizeof(msg));

Da bismo ispisali informacije koje smo pročitali sa socket-a, funkciji *print_can_frame()* šaljemo argumente kao na primjeru ispod: 

	struct can_frame * const frame = msg.frame;
	      unsigned char * const data = frame->data;
	      const unsigned int dlc = frame->can_dlc;
	      unsigned int i;
	      (..)
	      /* Print the received CAN frame */
	      printf("RX:  ");
	      print_can_frame(frame);
	      printf("\n");

## Filtriranje

Prilikom podešavanja opcija filtriranja primljenih poruka koriste se **RX** operacije (opcode) te postoji par različitih varianti koje možemo da implemetiramo.
Bitno je napomenuti da se u samoj strukturi značenje elemenata mijenja u zavisnosti od toga da li se koriste **RX** ili **TX** operacije.
Za **RX** operacije promjene značenja u odnost na **TX** su slijedeće:  

- count : nema/gubi značenje
- ival1 : vremenski okvir koji označava time-out interval (detaljnije prilikom objašnjenja te vrste prijema)
- ival2 : interval za prorjeđivanje **RX_CHANGED** poruka (poruka sa istim can_id)
- frame.can_data : maske za filtriranje podataka

### Filtriranje po ID-u 

Filtriranje na osnovu samog *can_id-a* postižemo tako što prilikom podešavanja filtera koristimo fleg **RX_FILTER_ID** kojim osiguravamo da će se filtriranje raditi isključivo na osnovu can_id-a te da se neće ispitivati sam sadržaj poruke.  

Implemetacija ovoga za sučaj **can_id = 0x0A0** prikazana je ispod.

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.flags   = RX_FILTER_ID;
	msg.msg_head.nframes = 0;  
	(..)
	write(s, &msg, sizeof(msg));

Funkcionalnost možemo da ispitamo korištenjem alata ***cansend*** te da se uvjerimo da jedini frejmovi koji se registruju na prijemnoj strani jesu frejmovi sa *can_id = 0x0A0*.  

### Time-out na prijemu

Podešavanjem vrijednosti parametra **ival1** u kombinaciji sa **RX** opcode-om možemo da podesimo vremenski interval u kojem naš prijemik očekuje da dobije određenu poruku. Ako prijemnik ne detektuje željenu poruku u datom vremenskom intervalu, procesu se šalje poruka sa opcode-om **RX_TIMEOUT** koja sadrži samo zaglavlje poruke bez podataka. Tajmer se ne pokreće automatski nego po prijemu datog frejma.  

Funckionalnost je pokazana na primjeru **can_id = 0x0A0** te vremenskim intervalom od **3[s]** i implementiran je kao: 

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.ival1.tv_sec = 3;
	msg.msg_head.flags   = RX_FILTER_ID | SETTIMER;
	msg.msg_head.nframes = 0;
	(..)
	write(s, &msg, sizeof(msg));

### Smanjenje učestanosti prijema

Prilikom brze promjene nekih podataka koji se primaju može možemo doći u situaciju da primamo podatke mnogo brže nego što možemo da ih obradimo te se tu javlja neka suvišnost u čitavom procesu. Kako bismo ovo riješili na prijemnoj strani možemo da podesimo da se poruke sa nekim *can_id-om* primaju samo u određenim vremenskim intervalima koji su dovoljno veliki da je obrada prethodno primljene poruke sa istim *can_id-om* moguća.  

Ovo postižemo tako što podešavamo parametar **ival2** na željenu vrijednost koja bi označavala gore pomenut vremenski interval. 

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.flags   = RX_FILTER_ID | SETTIMER;
	msg.msg_head.nframes = 0;
	msg.msg_head.ival2.tv_sec = 4; 
	(..)
	write(s, &msg, sizeof(msg));

Sa kodom prikazanim iznad poruke sa *can_id = 0x0A0* će da budu primljene na svakih **4[s]** bez obzira na to koliko proruka stigne prije isteka vremenskog intervala.

### Filtriranje na osnovu maske

Prilikom korištenja opcode-a **RX_SETUP** možemo da definišemo filter masku. Filter maska se definiše na prvoj poziciji u sekciji podataka poruke ( data[] ).
Prilikom višestrukih prijema poruke sa istim *can_id-om*, u slučaju da se detektuje promjena u nekom od bita od interesa (oni biti koji su naznačeni u data[]),
procesu se šalje poruka sa opcode-om **RX_CHANGED** koja je indikator da je došlo do promjene informacionog sadržaja poruke. 

Postavljanje filtera radis se na način:

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.flags   = 0;
	msg.msg_head.nframes = 1;
	U64_DATA(&msg.frame[0]) = 0x0000000000FF00FF; 
	(..)
	write(s, &msg, sizeof(msg));

Za konverziju podataka u *unsigned long logn* definisana je fukcija:  
	
	#define U64_DATA(p) (*(unsigned long long*)(p)->data)

Takođe bitno je navesti da je broj frejmova 1 te navesti masku za dati frejm. Na osnovu maske upisane u data[0] posmatraju se promjene podataka na bitima od interesa te prilikom promjene dobijamo prijem i ispis. Ako promjena nije detektovana ispisa neće biti.


### Filtiranje multipleksiranih podataka 

CAN_BCM nam pruža i mogućnost filtriranja poruka koje mogu da imaju više značenja odnosu na prvi bajt informacionog sadržaja. Kodovanjem prvog bajta možemo da dodijelimo različita značenja za ostalih 7 bajtova poruke. U filteru na prijemnoj strani možemo da definišemo tačno određene maske za kodovane bajtove od interesa i za sam informacioni sadržaj koji nas za te kodovane bajtove zanima.

Prilikom ovog slanja moraju se poslati barem 2 can_frame-a u kojem prvi sadrži MUX masku, a drugi poruku sa jedinstvenim identifikatorom kao prvim bajtom.
Ako je taj identifikator definisan u prijemnom filteru poruka će biti upoređena sa maskom i bitni podaci će biti primljeni i ispisani.
Ako identifikator poruke (prvi bajt) nije na listi definisanih u prijemnom filteru poruka će da bude ignorisana.

Primjer podešavanja filtra za multipleksiranje podatke:  

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.flags   = 0;
	msg.msg_head.nframes = 4;
	U64_DATA(&msg.frame[0]) = 0xFF00000000000000;
	U64_DATA(&msg.frame[1]) = 0x020F000000FF00FF;
	U64_DATA(&msg.frame[2]) = 0x4302F0035000FF00; 
	U64_DATA(&msg.frame[2]) = 0x7500FF0000FF0000; 
	(..)
	write(s, &msg, sizeof(msg));


U frame[0] definisali smo MUX masku kojom određujemo koji biti će da budu identifikacioni. S obzirom na tu činjenicu identifikacioni biti za frejmove su redom **0x02**, **0x43**, **0x75**.  

Slično kao i kod filtriranja na osnovu jedne maske iz prethodnog podnaslova posmatraju se primljeni frejmovi koji imaju odgovarajuć prvi bajt naveden u listi identifikacionih bita. Ako je potvrđeno da je identifikacioni bajt naveden u listi onda se provjerava da li je došlo do promjene na nekom od bita od interesa navedenih u ostatku maske. Promjena nekog od bita u odnosu na prethodno primljenu poruku sa istim identifikatorom rezultuje ispisom primljenog podatka. U suprotnom ne dolazi do ispisa.  

Takođe, do ispisa ne dolazi ako identifikator primljene poruke ne odgovara nekom od identifikatora navedenih u listi. 

