#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/bcm.h>

#define MSGID   (0x07B)
#define MSGLEN  (3)
#define NFRAMES (50)

#define DELAY (10000)

int main()
{
	int s,i;
	struct sockaddr_can addr;
	struct ifreq ifr;


/*struktura bcm_msg_head je definisana u okviru 
zaglavlja linux/can/bcm.h pa je ovdje ne treba definisati*/
 //    struct bcm_msg_head {
 //        int opcode;                   /* command */
 //        int flags;                    /* special flags */
 //        int count;                    /* run 'count' times with ival1 */
 //        struct timeval ival1, ival2;    /* count and subsequent interval */
 //        canid_t can_id;                 /* unique can_id for task */
 //        int nframes;                  /* number of can_frames in the next field */
 //        struct can_frame frames[NFRAMES];
	// };
/*Takodje struktura can_frame je definisana u okviru
linux/can.h pa je ni ovdje ne treba navoditi*/
	// struct can_frame {
	//         canid_t can_id;      /* 32 bit CAN_ID + EFF/RTR flags */
	//         __u8    can_dlc;     /* data length code: 0 .. 8 */
	//         __u8    data[8] __attribute__ ((aligned(8)));
	// };

    struct can_msg
    {
        struct bcm_msg_head msg_head;
        struct can_frame frame[NFRAMES];
    } msg;
	
	//Otvaranje BCM soketa 
	s = socket(PF_CAN, SOCK_DGRAM, CAN_BCM);
	if (s<0)
	{
		printf("Neuspjesno otvarnje\n");
	}

	/* Convert interface string "can0" to index */
	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);

	/* Setup address for connecting */
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	//connect fja se koristi u CAN_BCM, a bind u CAN_RAW
	if(connect(s, (struct sockaddr *)&addr, sizeof(addr))<0)
	{
		printf("Neuspjesna konekcija\n");
	}

    msg.msg_head.opcode  = TX_SETUP;
    msg.msg_head.can_id  = 0;
    msg.msg_head.flags   = SETTIMER | STARTTIMER;
    msg.msg_head.nframes = NFRAMES;
    msg.msg_head.count   = 0;

    /* Set the time interval value to 1200 ms */
    msg.msg_head.ival2.tv_sec = 1;
    msg.msg_head.ival2.tv_usec = 200000;

    /*Ciklicno slanje poruka */
    for (i = 0; i < NFRAMES; ++i)
    {
        struct can_frame * const frame = msg.frame + i;
        frame->can_id = MSGID;
        frame->can_dlc = MSGLEN;
        memset(frame->data, i, MSGLEN);
    }

    if (write(s, &msg, sizeof(msg)) < 0)
    {
       printf("Neuspjesno pisanje\n");
    }

    int k=0;
    
    /* Spin forever here */
    while (0 == k) usleep(DELAY);

	close(s);
	if (s<0)
	{
		printf("Neuspjesno zatvaranje");
	}
	return 0;
}	
