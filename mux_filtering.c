#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/bcm.h>

#define U64_DATA(p) (*(unsigned long long*)(p)->data)

void print_can_frame(const struct can_frame * const frame)
{
    const unsigned char *data = frame->data;
    const unsigned int dlc = frame->can_dlc;
    unsigned int i;

    printf("%03X  [%u] ", frame->can_id, dlc);
    for (i = 0; i < dlc; ++i)
    {
        printf(" %02X", data[i]);
    }
}

int main()
{
	struct ifreq ifr;				/* CAN interface info struct */
	struct sockaddr_can addr;		/* CAN adddress info struct */
	struct can_frame frame;			/* CAN frame struct */
	int s;							/* SocketCAN handle */

	struct {
	    struct bcm_msg_head msg_head;
	    struct can_frame frame[4]; 
	} msg;

	memset(&ifr, 0, sizeof(ifr));
	memset(&addr, 0, sizeof(addr));
	memset(&frame, 0, sizeof(frame));

	//opening BCM sockets
	s = socket(PF_CAN, SOCK_DGRAM, CAN_BCM);

	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);
	
	/* Setup address for connecting */
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	connect(s, (struct sockaddr *)&addr, sizeof(addr));
   

	msg.msg_head.opcode  = RX_SETUP;
	msg.msg_head.can_id  = 0x0A0;
	msg.msg_head.flags   = 0;
	msg.msg_head.nframes = 4;
	U64_DATA(&msg.frame[0]) = 0x00000000000000FF;
	U64_DATA(&msg.frame[1]) = 0xFF00FF0000000F02;
	U64_DATA(&msg.frame[2]) = 0x0002F0035000FF43; 
	U64_DATA(&msg.frame[2]) = 0x0000FF0000FF0075; 

	write(s, &msg, sizeof(msg));

   	while(1)
	 {
      ssize_t nbytes;

      /* Read from the CAN interface */
      nbytes = read(s, &msg, sizeof(msg));

	  struct can_frame * const frame = msg.frame;
      unsigned char * const data = frame->data;
      const unsigned int dlc = frame->can_dlc;
      unsigned int i;

      /* Print the received CAN frame */
      printf("RX:  ");
      print_can_frame(frame);
      printf("\n");
	 }
	close(s); 
	return 0;
}
